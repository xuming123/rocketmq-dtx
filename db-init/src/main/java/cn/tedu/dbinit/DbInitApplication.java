package cn.tedu.dbinit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.EncodedResource;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.sql.SQLException;

@SpringBootApplication
public class DbInitApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbInitApplication.class, args);
    }
    @Autowired
    DataSource dataSource;


    @PostConstruct
    public void excute() throws SQLException {
        run("sql/account.sql");
        run("sql/order.sql");
        run("sql/seata-server.sql");
        run("sql/storage.sql");

    }

    public void run(String msg) throws SQLException {
        ClassPathResource classPathResource = new ClassPathResource(msg, DbInitApplication.class.getClassLoader());
        EncodedResource encodedResource = new EncodedResource(classPathResource,"UTF-8");
        ScriptUtils.executeSqlScript(dataSource.getConnection() , encodedResource);

    }

}
